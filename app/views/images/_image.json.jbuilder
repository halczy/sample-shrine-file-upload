json.extract! image, :id, :description, :image_data, :imageable_id, :imageable_type, :created_at, :updated_at
json.url image_url(image, format: :json)
