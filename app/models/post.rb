class Post < ApplicationRecord
  include ImageUploader[:image]

  validates :content, presence: true
end
