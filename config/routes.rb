Rails.application.routes.draw do
  resources :images
  resources :products
  root 'posts#index'
  resources :posts
end
