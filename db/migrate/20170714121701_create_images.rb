class CreateImages < ActiveRecord::Migration[5.1]
  def change
    create_table :images do |t|
      t.text :description
      t.text :image_data
      t.references :imageable, polymorphic: true

      t.timestamps
    end
  end
end
